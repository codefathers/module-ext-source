<?php
/**
 * KUHN Product Configuraton System (PCS) Magento 2 Module
 * Copyright (c) 2017 KUHN Maßkonfektion KG
 * D-63936 Schneeberg
 * Website:    https://www.kuhn-masskonfektion.com
 * Contact:     info@kuhn-masskonfektion.com
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Cf_ExtSource',
    __DIR__
);
