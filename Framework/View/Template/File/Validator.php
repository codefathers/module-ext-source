<?php


namespace Cf\ExtSource\View\Template\File;


class Validator extends \Magento\Framework\View\Element\Template\File\Validator
{


    /** @var string */
    static $EXT_SRC_DIR;

    /**
     * returns true if file is accessible
     */
    public function isValid($filename)
    {
        if (strpos($filename, $this->getExtSrcDir()) === 0) {
            return file_exists($filename);
        }
        return parent::isValid($filename);
    }

    /**
     *
     * return string
     */
    protected function getExtSrcDir()
    {
        if (!isset(self::$EXT_SRC_DIR)) {
            self::$EXT_SRC_DIR = dirname($this->getRootDirectory()->getAbsolutePath('')).'/src/';
        }
        return self::$EXT_SRC_DIR;
    }

}
